---
layout: page
title:  "Configuration"
tags: Software Communication Protocol
---
               
## Example Hardware
                   
Two IO-Boards, two singals and four IR-Sender:

![UML Artifacts Overview]({{site.baseurl}}/assets/images/UML-Hardware.png)

# Configuration

![UML Artifacts Overview]({{site.baseurl}}/assets/images/UML-Config.png)
