---
layout: page
title:  "Communcation Protocol"
tags: Software Communication Protocol
---

The communication protocol is UDP based.

Each UDP package contains a message in form of a XML document.
These XML messages are defined in pi-rail-v1.xsd from which model classes are generated for C++ (firmware) and Java (app).

The following diagram shows communcation happening when modules get first time in contact with the controlling app:
 
![UML Sequence Communication]({{site.baseurl}}/assets/images/UML-Communication.png)

The folloing diagram shows an example of messages when user changes a signal and so a linked IR-Sender is updated.                     

![UML Sequence Communication]({{site.baseurl}}/assets/images/UML-ChangeSignal.png)